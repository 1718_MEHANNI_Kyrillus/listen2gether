# Listen2Gether

## About

Listen2Gether allows you to synchronise your favorite songs with your friends. It also has other common features such as playlists (with priorities for specific songs), youtube players and filtering.

## Component Diagram

![Component Diagram](images/compdia.png?raw=true "Component Diagram")

## Whom to ask?


Kyrillus Mehanni <meh17008@spengergasse.at>
